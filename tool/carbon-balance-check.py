#!/usr/bin/env python3
#
# Given a JSON document with Carbon Budget OpenData, check its totals.
#
# From: Rick van Rein <rick@groengemak.nl>


import sys

import json


if len (sys.argv) != 2:
	sys.stderr.write ("Usage: %s carbonbbudget.json\n" % (sys.argv[0],))
	sys.exit (1)


def collect (budget, valuekw):
	"""Find values in a JSON budget structure, prefixed by the given
	   value keyword.  Return the sum of the numeric values.
	"""
	totval = 0
	if isinstance (budget, dict):
		for k in budget:
			if k == valuekw:
				for val in budget [valuekw]:
					assert val ['units'] >= 0, 'No "units" value may be less than zero.'
					totval += val ['units']
			else:
				totval += collect (budget [k], valuekw)
	elif isinstance (budget, list):
		for sub in budget:
			totval += collect (sub, valuekw)
	return totval


budget = json.load (open (sys.argv [1]))

give   = collect (budget, 'give')
take   = collect (budget, 'take')
sink   = collect (budget, 'sink')
source = collect (budget, 'source')
margin = collect (budget, 'margin')
spare  = collect (budget, 'spare')

sum_out = sink + give
sum_in = source + take
sum_resv = margin + spare

print ('sum_out  = sink + give = %d + %d = %d' % (sink,give,sum_out))
print ('sum_in   = source + take = %d + %d = %d' % (source,take,sum_in))
print ('sum_resv = margin + spare = %d + %d = %d' % (margin,spare,sum_resv))

# Internal consistency
assert sum_out + sum_resv <= sum_in, 'The total values going out plus the total values reserved must not exceed the total values coming in.'

# TODO: External consistency


# Acknowledge
print ('Properly balancing accounts')
