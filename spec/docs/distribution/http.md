# Polling over HTTP

> *The HTTP protocol is mostly useful to download data on demand.  That is how it used here too.
> JSON objects may reference older JSON objects, or ones managed by peers, and so on.
> Being able to fetch anything required is useful.*

The requirement for HTTP is to have a fixed URL for the latest measurement, and a separate URL for any historic version of the JSON objects.  This is simple, and can be achieved with a static web server.  The signatures can be made out-of-band.

HTTP should be used without authentication.  It may use a TLS-secured protocol or plaintext.
