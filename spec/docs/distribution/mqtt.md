# MQTT for subscription/notify

> *The Internet-of-Things protocol MQTT offers subscription channels, and notifies of updates.
> New clients may receive the latest information.  Historic information may or may not be
> stored in MQTT, as at can also be downloaded over HTTP if need be.  This main point of
> using MQTT is to receive instant updates when they are available.*

MQTT should be used without authentication.  It may use a TLS-secured protocol or plaintext.
