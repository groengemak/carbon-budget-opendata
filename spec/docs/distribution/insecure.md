# In/Secure Protocols

> *Not all protocols need to be secure for the data to allow validation.*

Several places in this specification speak of in/secure URLs.  This is meant to be
read as "secure and/or insecure URLs".  This may raise the odd eyebrow.

The vital data for distribution is a PGP-signed JSON object.  The signature means
that the origin of the data can be independently validated, and a chain of trust
is built from a fingerprint near a trusted government.  This chain of trust makes
it unnecessary (at least for reasons of data authenticity) to use transport
protection with TLS, SSH or GSSAPI.

There are other reasons to run over TLS, namely privacy.  This is also a lesser
concern in this case, because the carbon budgets are supposed to be public.

Finally, the public nature of the data is also reflected in abandoning authentication
in all the protocols.

As a result, it is possible to use `http://` and `mqtt://` URLs instead of their
secure counterparts.  It is also quite possible to distribute the data as part of
an email, or perhaps as part of an artistic digital visualisation, or, if you liked,
even on a piece of paper hidden in the frame of a painted version.

So, the security that we all apply so mindlessly happens to be useless in this
case.  On the other hand, it is also harmless.  So if you have your HSTS setup,
your certificate rollouts automated and so on, then feel free to ignore this
mild nudge and go ahead publishing secure endpoints.
