# PGP Keys in DNS

> *PGP keys sign a message, making them independently verifiable, even after the time of download.
> The keys themselves must be connected to a domain name, which is not trivial.
> Fingerprints of keys can make that possible, when they are published in known-good places.*

Imagine the a client can trust the JSON object published by their own government.
If that is true, a person can chase links to parents, children, peers and specials.
Each of these comes annotated with a fingerprint, so any such steps to (possibly foreign)
other parties can be as reliable as the government, and that may work for the next one,
and so on.  We have a chain of trust.

The start of the chain of trust can rely on the domain name of the client's own government,
but may then still need manual work.  It is easier when that too is automated.  This can
be established with the DNSSEC infrastructure.  This is the signing of DNS data, using a
long-established chain of trust that starts in the technical root of the Internet naming
system DNS.

To use this, a DNS record with the PGP key fingerprint should be inserted into the domain
of the government.  This suffices to get their trust, and avoid the tedious comparison of
hexadecimal strings by humans.  This can be done with an
[CERT resource record](https://datatracker.ietf.org/doc/html/rfc4398).
The form IPGP (certificate type 6) is used to provide the fingerprint and URL of the
PGP key in DNS.  It can be found through Content-Based OpenPGP CERT RR Names, requiring
the domain name in the email address matches the well-known domain of the starting point
(local government) that the client is willing to rely on.
