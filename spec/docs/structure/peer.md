# Peers or Siblings

> *Besides relations between parent and child, there may also be side-ways
> relations.  These are known as peer links, perhpas comparable to siblings.*

In the object with the JSON data, any peers appear as a `"peer"` list of
party objects.  This list may be empty, but the `"peer"` key must be present.

```
{
  "meta": ...,
  "self": ...,
  "parent": [ ... ],
  "child": [ ... ],
  "peer": [
    {
      "domain": ...,
      "descr": ...,
      ...
    },
    ...
  ],
  ...
}
```


