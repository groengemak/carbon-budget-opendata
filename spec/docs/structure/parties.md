# Describing Parties

> *The various parties can be described by the online locations of their nodes,
> their names, their well-known domain name, their carbon budgets, and so on.
> We will see various kinds of parties, including the publisher's self-description.*

Every party is described by a JSON object, with mandatory keys.

  * `"domain"` (required) is the DNS name for which the party is widely known, and
    for which it should have implemented DNSSEC.  Under this domain, the validation of
    the fingerprint of a PGP key can be done.
  * `"descr"` (optional) is an informal description for the party, usually their name.
  * `"http"` (required) is the HTTP in/secure URL for the publication of the party's
    Carbon Budget Open Data.  No authentication is required to obtain the information.
    The URL is a fixed reference to the latest data available.
  * `"mqtt"` (required) is the MQTT in/secure URL for the publication of the party's
    Carbon Budget Open Data.  No authentication is required to obtain the information.
    This is not a web socket.  The URL is a fixed reference to the latest data available.
  * `"contact"` (required) is formatted as a `To:` header address, so an optional name
    and an email address between U+003C `<` and U+003E `>` symbols.
  * `"fingerprint"` (required) provides a list of strings, each a validated (so known-good)
    hexadecimal fingerprint of the public PGP key used by this party's publication node
   to sign their JSON objects.
   This information is not a public key on its own; it cannot be used to verify
   signatures.  It is a statement from the current node that it has validated
   all of this fingerprint information for the said party has properly validated the
   PGP key and computed this fingerprint value as part of the process.  This list
   should not be empty.  It is recommended to include as many old fingerprints, and
   possibly future fingerprints, to accommodate the future validation of open data
   published by the node of the described party.  This is especially valuable in a
   party description in which the publisher describes its own identity.


```
{
  "domain": "example.com",
  "descr": "Example Carbon-Responsive Government",
  "http": "https://wwwgov.example.com/carbon/budget.json.asc",
  "mqtt": "mqtt://broker.carbon.example.com/",
  "contact": "Demo Carbon Budget Node Signing Key <carbondemo@example.com>",
  "fingerprint": "0DB04837C2B2FDC1F4ED8F41FCEA3A576D1B1097"
}
```

**Visualisation.**  The purpose of having multiple kinds of parties is not to aid in
accounting, but rather in visualisation.  In terms of accounting it does not matter
who adds or subtracts, as long as it is clear which of the two they do.
