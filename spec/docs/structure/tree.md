# Tree of Parties

> *The structure for carbon accounting centers around territorial governments.
> To reflect this, the basic structure is a tree of parties, reflected in parent/child
> relations between (government) parties.  But there are pragmatic exceptions.*

The essential structure of governments is hierarchical, for instance a top-down
development could be:

  * World level: United Nations
  * Continent level: European Union, Asia, ...
  * Country level: the Netherlands, China, ...
  * Province level: Overijssel, Jiangsu, ...
  * Municipal level: Enschede, Nanjing, ...
  * Industrial level: Grolsch, ...

The idea is that each of these parties publish their own open data.  They form a
**Carbon Accounting Node** and the nodes connect.  The tree of parties consists
of parent references going up in this list, and child references going down.

The list above serves to sketch the idea, but it has no formal meaning.  The one
formal requirement is that there should not be cyclical dependencies in the
parent/child relations.  But take note however that if this happened, it would
not lead to accounting difficulties; some carbon might be cycling around without
being usable anywhere.

There are usually multiple child relation.  There may occassionally be multiple
parents, for instance when a country overlaps two continents, or when a city
crosses a province level.  This does not interfere with proper accounting, as
long as the various paths are considered separate and no budget is considered to
count once by one party yet counted multiple times by another.


