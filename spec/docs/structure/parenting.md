# Parent and Child

> *The parent and child relations form the default tree structure over which
> carbon budgets are most commonly passed around.* 

In the object with the JSON data, the children appear as a `"child"` list of
party objects, and the (usually, but not always one) parents appear as a
`"parent"` list of party objects.

```
{
  "meta": ...,
  "self": { ... },
  "parent": [
    {
      "domain": ...,
      "descr": ...,
      ...
    }
  ],
  "child": [
    {
      "domain": ...,
      "descr": ...,
      ...
    },
    ...
  ],
  ...
}
```

The keys `"parent"` and `"child"` are required, but the lists may be empty.
Such empty lists may occur for the root and leaves of the tree, but it may
also be used when no relation has been established.  Note however that there
are legal requirements to perform budgetting over emissions on a territory,
so this could mean that this tooling is not a sufficient implementation yet.

