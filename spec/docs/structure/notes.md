# Annotations

> *Throughout the open data, annotations can be made to guide humans.*

There are likely to be places where further details are desirable.  Such annotations have no formal ramifications in terms of accounting or legal meaning, but they may (for example) guide the reading of a document under a particular section.  Or it may expplain why a certain allocation was made in a particular manner.

The format for annotations is a list of strings, each of which count as a separate annotation, but which may form an ordered chain of reasoning.  Notes may reference earlier notes incrementally by numbering them from 0 for the first list entry.

Any object defined herein may be annotated with

```
"notes": [
    "Note 0...",
    "Note 1...",
    ...
]
```

The list may be of any length, even an empty list is permitted (and considered equivalent to not adding the `"notes: []"` element to the object).
