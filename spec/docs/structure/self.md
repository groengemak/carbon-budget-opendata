# Publishing Party Self-Reference

> *The simplest party in the open data is the publisher's self-reference.
> There must be exactly one of those.*

In the object with the JSON data, the publisher appears as `"self"`:

```
{
  "meta": ...,
  "self": {
    "domain": ...,
    "descr": ...,
    ...
  },
  ...
}
```
