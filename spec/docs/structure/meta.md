# Node Metadata

> *Every party publishes its own data.  Some of that data serves mostly to describe
> the data itself.  This is called meta-data.*

The data is structured as a JSON object.  In that dictionary, the `"meta"` key
indexes the meta-data, which itself is an object.

```
{
  "meta": {
    ...
  },
  ...
}
```

Unknown metadata fields may be ignored.  This allows future extensions.  Currently,
the following metadata fields are known:

  * `"version": [ `*major*`,`*minor*`,`*revision*`]`
    (required)
    describes the data version, derived from the Git tag on this repository,
    and the documents it holds.  The names *major*, *minor* and *revision*
    are for [semantic versioning](https://semver.org/).  An example would be
    `"version": [ 1, 0, 0 ]`

  * `"seqnr":` *integer*
    (required)
    is an ever-rising integer representing the version numerically.
    There is no problem if there are gaps in the numbering, but going back
    is likely to trigger errors in software and may even be construed as
    attempted forgery.  It is quite possible to use a UNIX timestamp, or
    a date-look-alike number, possible with time-look-alike values.  The
    integer is expected to be non-negative and have at least 63 bits for
    this range.  It may well be used as a (partial) database key.

  * `"prev": "`*url*`"`
    (required in all but the first)
    is the URL for the previous version of this document.  This can be used
    to collect a chain of historic data.  It should not be used to branch
    off a new, corrected version.  See `"correct"` for that.

  * `"correct": "`*url*`"`
    (required in all but the first)
    is the URL of the last correct version of this document, which must exist
    in the chain of `"prev"` references.  Using this it is possible to skip
    zero or more publications that were wrong in retrospect.  This is the
    well-defined mechanism for correcting mistakes, even when data has been
    distributed already.

  * `"pubkey": [ `*string*`, ... ]`

    (required)
    a list of of one or more strings holding HTTP in/secure URLs, from which the public PGP key
    can be downloaded that was used to sign this information, represented in
    ASCII-armoured notation without surrounding markup or text, so as to facilitate
    automatic key handling.  Think of the process used to publish PDF documents,
    where the raw document is made available.  This can be used to verify the PGP
    signatures on JSON, to track consistency between signed open data packages.
    A public key is not sufficient to validate the coupled identity of the PGP key,
    so that needs separate validation, using fingerprints or trust signatures by
    known-good PGP keys.  To simplify trust validations, the public PGP key in
    the said locations is strongly recommended to obtain trust signatures from
    the parties to which it relates.  It is also recommended to distribute proper
    validation points under known official domains, if these use DNSSEC to build
    a chain of trust to these validation helpers.  Of course, publishing a PGP
    fingerprint on an official domain is also helpful, but only supportive of
    manual validation.

Note that the publisher is not described; there will be a `"self"` party that
holds a description.


An example of the published object with meta-data could be

```
{
  "meta": {
    "version": [ 1, 0, 0 ],
    "seqnr": 100,
    "prev": 99,
    "correct": 99,
    "pubkey": [
      "https://a.example.com/our-node-key.asc",
      "https://friends.example.net/party-a-node-key.asc"
    ],
  },
  ...
}
```
