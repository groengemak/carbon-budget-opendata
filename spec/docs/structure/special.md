# Special Relations

> *There is a need to represent arbitrary relations that are neither part of
> the parenting relations nor should be represented as peers.  For instance,
> a relation to a large industry under one's responsibility.*

In the object with the JSON data, any peers appear as a `"peer"` list of
party objects.  This list may be empty, but the `"peer"` key must be present.

```
{
  "meta": ...,
  "self": ...,
  "parent": [ ... ],
  "child": [ ... ],
  "peer": [ ... ],
  "special": [
    {
      "domain": ...,
      "descr": ...,
      ...
    },
    ...
  ],
  ...
}
```

The `"special"` keyword is required, but the list may be empty.


