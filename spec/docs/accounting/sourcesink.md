# Source and Sink

> *Carbon budgets appear in some places like swamps and forests.
> They disappear in places like highways, airports and industry.
> These are defined respectively as sources and sinks of carbon budgets.*

**Teminology.**
This is budget accounting, and a source of the budget is not the source
of GHG, but rather a place where it vanishes.  Likewise, the sink of a
budget is not a sink of GHG, but rather a place where it is emitted.

**Budget sinks.**
Parties must account for all sources of carbon budgets in their territory using
`"sink"` keys in `"special"` or `"self"` parties, but double accounting should be avoided.
For this reason, part of a parent/child
relation must be an agreement where emissions on the territory is added
to the accounting model.  Neglecting this is a failure on the parent party.

A good example is a highway, which runs within a province but may span
multiple municipals.  The province is ultimately responsible, but may have
gotten their budget delegated under the condition that the highway, inasfar
it is part of the child's territory, is accounted for as a `"source"` of
CO₂eq emissions.  This delegation of responsibilities may be in multiple
steps.

**Budget sources.**
Parties may account for sinks of carbon budgets in their territory using
`"source"` keys in `"special"` parties to find values, but only inasfar
as it exceeds the sources that were available in the reference year 1990.
This is also a top-down responsibility, where a parent may need to cancel
fewer sources in one territory with those that have grown in another.  This
may again be part of the delegation of the carbon budget.

In general, any `"source"` should be monitored and is subject to external
review at any time.  This is a place where the system may be abused, and
it will be increasingly motivating to do so.  Do not trust `"source"`
values lightly.

**Root node.** The ultimate `"source"` value is determined by the root node
of the parenting tree, as part of their self-descriptive `"self"` object.
This should be properly documented, and only done by a party that is probably
the United Nations.  Its documentation is probably subject to signing by
states for the distribution of the budget and by the scientist panel of the
IPCC for the total budget being sourced.
