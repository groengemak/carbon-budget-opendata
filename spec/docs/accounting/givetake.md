# Give and Take

> *Parties may relate to each other by giving and taking parts of their carbon budget.*

Parties may `"give"` a part of the carbon budget to others.  Those others may `"take"`
it.  Taking what was not given is stealing, of course, and is forbidden by the rules
of accounting.

The `"give"` and/or `"take"` values may occur in `"peer"` and `"special"` parties,
as well as in values in `"parent"` and `"child"` nodes.


## Capturing Policy in Models

It is possible to model carbon emissions as they result from policy.
This could be used in proofs or simulations to test if the World (or a party)
is doing a good job in general.

TODO: Choose from options:

  * The most complex form is to encode the policy in (say) Python3.
    These might be passed through a Monte Carlo simulation to see if all goes well.

  * Much simpler is a linear local approximation, which connects incoming `"take"`
    to outgoing `"give"`.  This is likely a differential that indicates how each
    `"give"` value depends on all `"take"` values.  It would be trivial to detect
    if that can lead to negative carbon budget.

  * Slightly more complicated models might incorporate tensors or polynomials.
    Be careful not to smear the budget over parties that need to work it out
    together; always direct to retain responsibility.

Note that models can be updated at any time with a new upload.  Their value is
mostly in simplifying the handling of small changes, and to avoid the necessity
of immediate responses to upstream changes, which may cause a ripple effect and
possibly even infinite cycling of updates.

Note that a model implies that a party that relies on a `"take"` is made
responsible of doing these computations based on the `"give"` values elsewhere
in the network.

Especially the localised linear model can be easily combined into an overall
model that can be quickly analysed to keep the carbon budget non-negative.
