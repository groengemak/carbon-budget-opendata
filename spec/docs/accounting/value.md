# Documented Values

> *Having set a unit of accounting, we now turn to the values that may be stored.
> Since this administration is likely to have legal ramifications, there will be
> a need to reference documentation, such as government decisions, predictions,
> measurements and so on.  This means that a value can be a complex thing.*

The number of budget units is generally described with a list of values, each
of which must be a non-negative integer number of grams CO₂eq of remaining
cumulative emissions.  And each of these can be annotated.

The key preceding a value indicates what function the values have in an object,
but that is not an issue of concern here.  We need to specify the values as a list
of integer values that are to be summed, `[`*value1*`,`*value2*`,...]` basically.
Each value in turn is an object.

```
{
  "units": 1234567,
  "docs": [ ... ]
}
```

Following the `"docs"` key is a list of objects that specify a kind of document
and a publication location:

```
{
   "kind": "audit",
   "url": "*url*",
   "hash-sha512": "*hexvalue*"
}
```

There can be many values for the `"kind"`, as defined in the master branch of this
repository, as described below.  The purpose is to have a light-weight process of
expansion of these values, while staying mindful of proper accounting so as to have
technical definitions that automatically assure the requirements of the human rights.

The form `"hash-`*algorithm*`"` captures a secure hashing algorithm, generally named
as in *algorithm*, in lowercase and without dashes.  The value behind this key in
*hexvalue* is the hexadecimal representation of the secure hash over the document at
the *url*.  Multiple *algorithm* choices are possible, and each document must always
carry at least one that is considered by this specification or a later update as a
sufficiently secure hash, in terms of crypto-analytical attacks.  Until further
notice, support of at least the value `"hash-sha512"` is required.


## Register of Document Kinds

Following is (a copy of) the register of document kinds.  The
[authoritative document publication location](https://carbon-budget-opendata.groengemak.nl/accounting/value/)
of this document is leading, and can only be changed through delegation from
the authoritative location by putting another link in the above link at that location.

Proposals for new document kinds can be proposed via merge requests and may be
subjected to some discussion.  The purpose is to avoid, as much as possible,
any overlap or vagueness in the documentation kinds.  In the end, consistency is
a constraint for the accounting system as a whole and any overlap must warrant
that proper accounting without "leakage" can still be achieved.  Do not wildly
assign new documentation kinds as that will void commpatibility and is likely to
be a legal liability with respect to human rights.  Instead, open up for discussion.

New proposals may be entered in the
[issue tracker](https://gitlab.com/groengemak/carbon-budget-opendata/-/issues)
and should be accompanied by a
[merge request](https://gitlab.com/groengemak/carbon-budget-opendata/-/merge_requests)
on the `master` branch in the
[authoritative document source tree](https://gitlab.com/groengemak/carbon-budget-opendata/-/tree/master).

The following document kinds are currently registered:

  * `"audit"` defines an allocation of a budget on `"give"` values, and a
    use of the allocation on `"take"` values.

  * `"proof"` defines the mechanisms by which a `"source"` value comes about,
    and accurate measurements from which their values are derived.  These
    documents must fall under scientific scrutiny of assurance, and they may
    be updated as knowledge progresses.

  * `"exhaust"` defines a measurement of systems that sink parts of the carbon
    budget through exhaust of carbon emissions.  These documents must fall under
    scientific scrutiny of assurance, and must derive cumulative exhaust with
    respect to the same period as the carbon budget that it consumes from.
    These documents are subject to regular updates, and must define such terms.
