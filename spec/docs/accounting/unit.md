# Unit of Account

> *Proper accounting requires a clear link to the reality being expressed
> by the unit in accounting calculations.*

This open data format is driven by the desire to implement the human rights
concerning climate change.  Given this purpose, the name **Carbon Budget**
cannot be interpreted as strictly CO₂, but should encompass all GHG.  We
should compute with the unit CO₂eq.

We shall set the value of 1 **Accounting Unit** to the climate impact of
1 g CO₂eq.  Note that budgets may vary over time in a different manner
depending on the nature of the actual GHG computed.  Methane would start
higher and drop more quickly than CO₂, for instance, and that might show
up in the `"sink"` and `"source"` properties.

The unit is very low compared to everyday CO₂ emissions, but since many
things need to reduce to zero, it is still a good idea to measure so
accurately.  We can perform subsequently do accounting on integer numbers.
This has the added benefit of avoiding floating-point errors or reduced
precision on very large numbers.  It is assumed that integers are at
least 64 bits in size, with 63 bits available for non-negative numbers.
This easily accommodates the carbon budget that is still left.

The avoidance of negative numbers reduces any risk of accidents in the
specifications.  To allow for transport of the budget in two directions,
this specification defines separate keys for the directions.  It also
treats them differently; incoming budget needs to be validated, and we
do not want to miss that if it occurs as a negative outgoing budget.
Negative numbers are always a problem in this data format.

Moreover, there is a need to compare different statements from different
parties, possibly given at different moments.  To that end, it is good
to have these values separately, so as to impose clear restrictions on
each direction without conflating it with the opposite flow.

**Not emissions:** The units express a budget, so the remaining cumulative GHG
that may be emitted.  It does not indicate annual emissions, for example.  The
values are bound to get lower and lower over the years.  When policies aiming
to reduce annual reductions fail, then the number of units of remaining GHG
emissions drop will faster.

**Planning tool:** The intenion of a carbon budget is to have a planning tool
that makes it more tangible how fast the emissions reductions need to be,
and who is or is not acting according to standards.  It may also point out the
parties involved in warfare or other climate-destructive actions that need to
change for them to align with other parties.

**Overshoot:** Even when overshoot occurs, it must not allow negative numbers.
Any overshoot is critical because it may trigger tipping points, endangering
the human rights for which this budgetting is vitally important.  Overshoot,
if any, must be explicitly budgetted and distributed via normal accounting
rules to make the painful situation completely obvious and to show clearly
at the parties responsible (because they need to act, or have the economies
curtailed, perhaps with trading embargoes, to reduce their emissions).
