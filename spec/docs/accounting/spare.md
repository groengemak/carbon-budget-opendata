# Spare Budget for a Party

> *There may be an amount of spare budget, which may be expressed explicitly in
> any object with an optional extra key.*

Every object that performs accounting may be setup with a spare budget by adding
an extra key `"spare"` with the non-negative numeric value.  This value is never
obliged, as the accounting rules accept any non-negative budgetting structure.

If used, the `"spare"` key can be added to any party object, or to the overall
object, to index a values array with any budget that is currently neither used
nor planned, not even as an uncertainty marin.
