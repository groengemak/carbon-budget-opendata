# Accounting for Uncertainty

> *As an assurance for uncertain developments, accounting should handle
> uncertainty with reserves.  This is not any different from financial
> accounting.*

The rules of accounting used here are strict and demand that no budget
is ever exhausted to drop below zero.  Since publication of open data
will be sitting around for a while before their next version, it is
necessary to avoid negative balances in the mean time.

The rule now is that statistics must be used to have 95% assurance that
no balance is overdrawn from the current publication to the next.  There
is no limitation on how often this open data is published, so in case of
a problem it can be immediately corrected, but there will also be some
processing time in children or peers and for that reason there may be a
need for leniency.  The uncertainty margin is there to accommodate such
situations.

If used, the `"margin"` key can be added to any party object, or to the overall
object, to index a values array with any budget with the sole purpose of
avoiding overdrawing the carbon budget.
