# Rules of Proper Accounting

> *Having defined the data structures, we can now define the rules of proper accounting.*

The [units of account](../unit) are grams (not kilograms) of CO₂eq, and they must be
expressed in non-negative integer numbers.

  * The total values going out are listed after `"sink"` and `"give"` keys.
  * The total values coming in are listed after `"source"` and `"take"` keys.
  * The total values reserved are listed after `"margin"` and `"spare"` keys.

Numerical consistency assumes non-negative values:

  * **No `"units"` value may be less than zero.**

Internal consistency of accounting requires:

  * **The total values going out plus the total values reserved must not exceed the total values coming in.**

Extenal consistency of accounting requires:

  * **The total values coming in from a party must not exceed the total values that this party has in their administration as going out towards our party.**

Documentation trails are to be used to audit the local environmental sanity:

  * **Documents must validate any values after `"source"` keys.**

  * **Any `"take"` keys must reference the same documentation, with at least one matching secure hash that this document and its follow-ups consider sufficiently secure against crypto-analytic attacks as the matching `"give"` keys.**

