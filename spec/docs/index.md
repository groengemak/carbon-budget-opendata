# Open Data for Carbon Budget Accounting

> *This specification details how carbon budgets can be accounted for, among independent
> government parties, and how they can regularly publish this data such that accounting
> can be done by independent outsiders.  This helps governments to implement human rights.*

The structure of this document is:

  * **Structure** explains how the open data packets are structured, and how they relate.
  * **Accounting** explains how accounting takes place within data packets, as well as
    between them.
  * **Digital Signing** explains how data can be verified as being authoritative after it has
    been retrieved.  It also explains the pragmatics of handling temporary flaws.
  * **Data Formats** explains the representation of the data in PGP-signed JSON for
    distribution, in CSV tables for local use and SQL table sets for interactive
    queries.
  * **Distribution** explains how the open data can be published in a standard manner,
    and how the open data structure connects the publisher nodes.

[This document comes with tools](https://gitlab.com/groengemak/carbon-budget-opendata/)
to implement the open data specification.

Scroll forward for a [quick example](formats/json) of the exchange format.
