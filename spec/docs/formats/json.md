# PGP-signed JSON

> *The data format for public exchange is signed data, so it can be validated as desired.
> The signing is done with the flexible and solid PGP system, where JSON is fixated
> in an ASCII representation.*

## Example JSON object

The JSON format takes a form as described in this specification.  An example is given in
`text/example.json` (which may develop alongside this open data specification).
We include a dump by way of a demonstration.

```
{
  "meta": {
    "version": [
      0,
      1,
      0
    ],
    "seqnr": 100,
    "prev": 99,
    "correct": 99,
    "pubkey": [
      "https://a.example.com/our-node-key.asc",
      "https://friends.example.net/party-a-node-key.asc"
    ]
  },
  "self": {
    "domain": "example.com",
    "descr": "Demo Carbon Budget Open Data",
    "spare": [
      {
        "units": 67,
        "docs": []
      }
    ]
  },
  "parent": [
    {
      "domain": "example.net",
      "descr": "Uneted Netions",
      "source": [
        {
          "units": 1234567,
          "docs": []
        }
      ]
    }
  ],
  "child": [],
  "peer": [
    {
      "domain": "example.org",
      "descr": "Carbon Budget needing peer node",
      "give": [
        {
          "units": 1234000,
          "docs": []
        }
      ],
      "margin": [
        {
          "units": 500,
          "docs": []
        }
      ]
    }
  ],
  "special": []
}
```

JSON can be compressed into a single line if so desired.  This is not different
in terms of JSON, but it matters to PGP.  So once it is signed, do not change
that, but instead treat the textual form as fixated.  Otherwise, the signature
will fail to validate the JSON object.  Once the signature is validated, this
is no longer a concern and the JSON object can once again be reformatted.  The
concern should be taken into account while publishing the data, but this is
simple enough; just consider it fixed like a PDF is, and provide it as a raw
file rather than making people extract it with copy/paste.

## Example PGP-signed JSON object

When signed with PGP, the same data would look like this:

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

{
  "meta": {
    "version": [
      0,
      1,
      0
    ],
    "seqnr": 100,
    "prev": 99,
    "correct": 99,
    "pubkey": [
      "https://a.example.com/our-node-key.asc",
      "https://friends.example.net/party-a-node-key.asc"
    ]
  },
  "self": {
    "domain": "example.com",
    "descr": "Demo Carbon Budget Open Data",
    "spare": [
      {
        "units": 67,
        "docs": []
      }
    ]
  },
  "parent": [
    {
      "domain": "example.net",
      "descr": "Uneted Netions",
      "source": [
        {
          "units": 1234567,
          "docs": []
        }
      ]
    }
  ],
  "child": [],
  "peer": [
    {
      "domain": "example.org",
      "descr": "Carbon Budget needing peer node",
      "give": [
        {
          "units": 1234000,
          "docs": []
        }
      ],
      "margin": [
        {
          "units": 500,
          "docs": []
        }
      ]
    }
  ],
  "special": []
}
-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEEDbBIN8Ky/cH07Y9B/Oo6V20bEJcFAmZJJTYACgkQ/Oo6V20b
EJcNWwwAqBIayo93o0XbWaxwHBWgeF/CEY9KvjcJ1pQTzajzp0NtbM/SNK0MMuj4
HnOOyOhY/R3pLZlgEN+HlSQ4tHgDqMYGZmHXoO+T66YAFOLCEuuNZzQ113gHa9cW
wLzgRCBCIJss1BUYekPxo9dx0q5/0OiOQeZ/6TQ3FGUeyS5XNznde2nnbuKT2Hkx
4YmSB+rzmU0HTQGE0jU2B6aLgLZS5Is5lwicJxknf5KRrh8k//AJCef70sGsf3Eg
Jt5WltkhED/bHdX90EEJ6sNWBM0ZQ3BE+RkyuAy1Uul0Fj+zYrX+dBtwc3Y+0sGH
RljEm/9a3NhrlZl68W7HvCijtXhttkC6o2n88QTpvRXM7QSyNKATr7OzgEkA2vAT
b5ZKMpQIvEKDQ5Zr9+El2XrxjyyiziKaEqP1HeQam1NjqNu4qnIEap4ZjVE7H5wJ
rrkKBNO0I+iWKo8nlkYbmIwWfJxHXrnqsb3E4W2V9lkSvDShuMA3z2xSwIlrRGBY
v3B6MY7f
=UXeI
-----END PGP SIGNATURE-----
```

It should now be treated as a file that cannot be changed, precisely
because that would impair the authenticity; that is desired, because
changes may be a hint of tampering with the data.

