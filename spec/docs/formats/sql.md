# Structured Query Language

> *SQL is a standard language for databases, and we provide a format for creating
> tables and inserting data.  Different than with CSV, there can be multiple
> tables, so as to avoid duplication of elements.*

Duplication is avoided by separation of tables.  An internal `id` field is created,
and looked up before duplication would create another entry with the same data,
which is usually possible with a `CREATE IF NOT EXISTS` command.  In other statements,
it is then possible to `SELECT` that particular entry, be it new or not, and use it
in any number of rows as a foreign key in an `INSERT` statement.

TODO: Details.
