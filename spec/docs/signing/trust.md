# Establishing Trust

> *PGP keys validate signatures to see if a document is authentic, that is that its
> source is the expected source and tha no tampering has changed the document while
> it was in transit.  This is not the same as trusting the key to be tied to the
> identity of the authority said to have signed it.*

The fingerprint, which is the long sequence `0DB04837C2B2FDC1F4ED8F41FCEA3A576D1B1097`
in the examples above, and a different sequence of similar structure for your own,
is a shorter version of the key that is so unlikely to occur twice for different keys
that it is a useful code to compare to the keys to assure that you are using the right
key.  Some options for publishing those are on a well-known secure website (for manual
checking), in DNS or, for references between nodes, in the JSON object as part of the
reference of each party (including even the `"self"` party description).  This is the
path by which parents, children, peers and specials can be trusted.

Before including others' fingerprint in the JSON data, you should assure that it is the
correct code.  This may be done by looking it up in the DNSSEC-protected DNS of the
party's known domain name.  It may be done by calling a representative of the other
node, reading out the fingerprint and carefully comparing it.  Skipping this phase is
taking a risk with the linkage between data, so this one-time effort is quite valuable.
Also, it only needs to be done between nodes that reference each other, so among parents,
children, peers and specials.  All the users benefit from the careful validation.


