# Formal Meaning

> *Signatures can have any number of meanings.  Some are legal, others merely state that
> a piece of code was not changed since it was released.  What is meant by the signatures
> on a JSON object with Carbon Budgetting open data?*

The government or other party who signs a JSON object states that it is convinced that this
is the data to fare on, and that its budgets assigned to others via `"give"` and such are
solid and reliable.

But it is human to err, and civil servants can also be human.  Errors should be considered
things that may be corrected.  This means that any inconsistencies, such as improper
accounting or cutting short the carbon budget of a peer or child can be fixed when complaints
come in.  Beyond that, the data should be considered hard evidence in situations concerning
human rights in the sense of mitigation of climate change.

