# Corrections of Wrong Data

> *When data was published wrongly, it can be helpful to fix it immediately.
> This idea is not to rewrite history, but merely to avoid clutter in recent
> data by replacing it.*

Data should not be retracted; the notification mechanisms of MQTT work fast,
and databases may have been filled with data indexed by the `"seqnr"` in the
`"meta"` part.

The alternative method is simply to release new data as soon as possible,
while keeping the old data but indicating it is a hole in the correct flow.
This is only supported for the most recent JSON objects.

There are two chains of information concerned.  In the meta data, the
`"prev"` reference points to the latest published JSON object before this
one, be it right or wrong; the `"correct"` reference points to the last
published correct JSON object, and it may skip a few recent JSON objects.
It will not include new historic versions, but will instead inject a new
current JSON object and by the `"correct"` reference drop the false ones.
