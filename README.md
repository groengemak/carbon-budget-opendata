# Carbon Budget OpenData

> *This is a small project that specifies an open data format for carbon budgetting.
> In addition, this project develops stable open source software that implements
> software to publish these budgets.  The result is intended to comply to the
> human rights in terms of carbon budget management and can therefore help territorial
> governments.  At the same time, it satisfies the growing need for this information
> and accounting desires related to it.*

**This project is not finished yet.  We are developing our understanding of needed structures.**

This project provides:

 1. **JSON data structures** for distributing Carbon Budgets
 2. **HTTP and MQTT** setups to distribute these JSON objects
 3. **CSV import and export** software in Python3 to produce these JSON objects
 4. **OpenOffice Calc spreadsheets** with example CSV data structures
 5. **OpenOffice Database setup** (perhaps) for interaction with CSV/JSON objects

The [JSON structure specification](https://carbon-budget-opendata.groengemak.nl/)
can be rebuilt anywhere using
[mkdocs](https://www.mkdocs.org/),
which will be automatically done upon checkin into this repository.

The HTTP setup relies on the popular
[Apache HTTPD](https://httpd.apache.org/)
web server to distribute the JSON objects as static files.
This is intended for polling use, and may be used in JavaScript applications.

The MQTT setup relies for the proven
[Mosquitto](https://mosquitto.org/)
broker software, which allows subscribers to be instantly notified.
Such subscribers can subscribe from any client, for example using Python3,
and implement policy guidelines in that client to accommodate automatic
updates to the carbon budget.

## Purpose and Structure

The idea of a carbon budget is to **account for the decay of the carbon budget**
as a result of GHG emissions, and to allow an orderly process of tuning our lives
without having to go overboard in fear or uncertainty.  This is achieved by accounting
the carbon budget in a set of related territorial or otherwise involved parties who
publish their remaining budgets, and see their emissions subtracted.

Policy must manage this carbon budget accounting, to mitigate the dangers of climate
change when global warming surpasses 1.5 ⁰C.  Having **fast feedback and expedient updates**
allows policies to fine-tune their understanding and do what is necessary without
having to go overboard.  As long as legislation is prepared to support fast and responsive
corrections it should be possible to avoid mayhem, uncertainty and panic.

The idea is that each territorial government, and possibly private parties as well, as
**responsible parties run a node that emits open data to the public** as JSON structures,
while being bound by its limitations.  Anyone can verify the relations between the nodes
and detect any shortcomings.  Some nodes can distribute the budget over smaller parties,
and any might use peering relations to correct for import and export, and all this can
be modified in a plan that can be (automatically) updated under a predefined policy that
is implemented in the distributing software.  Such policies operate directly on the
JSON structures.

There is no limitation to who might use these tools.  It could span the World as well
as just the Netherlands or only Enschede.  In general, the idea of open data is to make
the exchange and accounting of the data simpler; furthermore, the use of open source makes
it an attractice solution to address a legal requirement; finally, the use of sharing
infrastructure from open source facilitates collaboration and progress of this toolkit.
If Linux can be built as a full alternative to commercial Windows by making random unpaid
developers collaborate through these forms of sharing, then it should also suffice to
make motivated governments and the open source community collaborate on a united goal to
abandon fossil fuels and replace them with sustainable alternatives.

Another intention is to prepare for **future processing of daily budget updates** from the
[CO2M instrument from ESA](https://www.esa.int/ESA_Multimedia/Images/2022/03/CO2M)
which will be launched in early 2025 with data expected from 2026.

The JSON objects are published **with a digital signature**, so as to
support formal assurance that the data is valid, regardless of the medium of
transport.  If data is later detected to have been falsely sent, perhaps due
to technical reasons, it may simply be corrected with a new publication.  The
JSON format contains a mechanism for defaming a prior post in a new publication.

The nodes maintain their **history of published JSON structures**, so as to support
historic analysis and the creation of graphics about progress with time.  Any
defamed JSON structures should ideally not be taken into accoun in such historic
analysis, as it might distract from the intention of the analysis or reporting.

