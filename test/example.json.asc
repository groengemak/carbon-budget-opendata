-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

{
  "meta": {
    "version": [
      0,
      1,
      0
    ],
    "seqnr": 100,
    "prev": 99,
    "correct": 99,
    "pubkey": [
      "https://a.example.com/our-node-key.asc",
      "https://friends.example.net/party-a-node-key.asc"
    ]
  },
  "self": {
    "domain": "example.com",
    "descr": "Demo Carbon Budget Open Data",
    "spare": [
      {
        "units": 67,
        "docs": []
      }
    ]
  },
  "parent": [
    {
      "domain": "example.net",
      "descr": "Uneted Netions",
      "source": [
        {
          "units": 1234567,
          "docs": []
        }
      ]
    }
  ],
  "child": [],
  "peer": [
    {
      "domain": "example.org",
      "descr": "Carbon Budget needing peer node",
      "give": [
        {
          "units": 1234000,
          "docs": []
        }
      ],
      "margin": [
        {
          "units": 500,
          "docs": []
        }
      ]
    }
  ],
  "special": []
}
-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEEDbBIN8Ky/cH07Y9B/Oo6V20bEJcFAmZJJTYACgkQ/Oo6V20b
EJcNWwwAqBIayo93o0XbWaxwHBWgeF/CEY9KvjcJ1pQTzajzp0NtbM/SNK0MMuj4
HnOOyOhY/R3pLZlgEN+HlSQ4tHgDqMYGZmHXoO+T66YAFOLCEuuNZzQ113gHa9cW
wLzgRCBCIJss1BUYekPxo9dx0q5/0OiOQeZ/6TQ3FGUeyS5XNznde2nnbuKT2Hkx
4YmSB+rzmU0HTQGE0jU2B6aLgLZS5Is5lwicJxknf5KRrh8k//AJCef70sGsf3Eg
Jt5WltkhED/bHdX90EEJ6sNWBM0ZQ3BE+RkyuAy1Uul0Fj+zYrX+dBtwc3Y+0sGH
RljEm/9a3NhrlZl68W7HvCijtXhttkC6o2n88QTpvRXM7QSyNKATr7OzgEkA2vAT
b5ZKMpQIvEKDQ5Zr9+El2XrxjyyiziKaEqP1HeQam1NjqNu4qnIEap4ZjVE7H5wJ
rrkKBNO0I+iWKo8nlkYbmIwWfJxHXrnqsb3E4W2V9lkSvDShuMA3z2xSwIlrRGBY
v3B6MY7f
=UXeI
-----END PGP SIGNATURE-----
